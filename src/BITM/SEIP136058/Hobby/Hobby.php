<?php

namespace App\BITM\SEIP136058\Hobby;

use App\BITM\SEIP136058\Utility\Utility;
use App\BITM\SEIP136058\Message\Message;

class Hobby{


    public $id= '';
    public $hobby= '';
    public $conn;
    public $deleted_at;

    public function prepare($data = "")
    {
        if (array_key_exists("hobby", $data)) {
            $this->hobby = $data['hobby'];
        }

        if (array_key_exists("id", $data)) {
            $this->id = $data['id'];
        }

    }


    public function __construct()
    {
        $this->conn=mysqli_connect("localhost","root","","atomic_project");
    }

    public function store()
    {
        $query = "INSERT INTO `atomic_project`.`hobbies` (`hobby`) VALUES ('" . $this->hobby . "')";
        //echo $query;

        $result = mysqli_query($this->conn, $query);
        if ($result) {
            Message::message("
                <div class=\"alert alert-success\">
                            <strong>Success!</strong> Data has been stored successfully.
                </div>");
            Utility::redirect("index.php");
        } else {
            echo "Error";
        }
    }

    public function index()
    {
        $_allHobbies = array();
        $query = "SELECT * FROM `hobbies` WHERE `deleted_at` IS NULL";
        $result = mysqli_query($this->conn, $query);
        while ($row = mysqli_fetch_object($result)) {
            $_allHobbies[] = $row;
        }

        return $_allHobbies;
    }

    public function view(){
        $query = "SELECT `hobby` FROM hobbies WHERE `id`=".$this->id;
        $result = mysqli_query($this->conn,$query);
        //Utility::d($result);
        if($row = mysqli_fetch_assoc($result)){
            //Utility::d($row);
            $singleHobby = explode(",",$row['hobby']);
            // Utility::d($singleHobby);
            return $singleHobby;
        }

        else
            echo "ERROR!";
    }

    public function edit(){
        $array =array();
        $query = "SELECT * FROM `hobbies` WHERE `id`=" . $this->id;
        $result = mysqli_query($this->conn,$query);
        if($row = mysqli_fetch_assoc($result)){
            $array[]= explode(",", $row['hobby']);
            //echo $row['hobby'];
           // var_dump($array[0]);
            return $array[0];
        }
        else
            echo "ERROR!";
    }

    public function update()
    {
        echo $this->hobby.$this->id;
        $query = "UPDATE `atomic_project`.`hobbies` SET `hobby` = '".$this->hobby."' WHERE `hobbies`.`id` = ".$this->id;
        //echo $query;

        $result = mysqli_query($this->conn, $query);
        if ($result) {
            Message::message("
                <div class=\"alert alert-success\">
                            <strong>Success!</strong> Data has been updated successfully.
                </div>");
            Utility::redirect("index.php");
        } else {
            echo "Error";
        }
    }

    public function delete()
    {
        $query = "DELETE FROM `atomic_project`.`hobbies` WHERE `hobbies`.`id` = " . $this->id;
        $result = mysqli_query($this->conn, $query);
        if ($result) {
            Message::message("
<div class=\"alert alert-info\">
  <strong>Deleted!</strong> Data has been deleted successfully.
</div>");
            Utility::redirect("index.php");
        } else {
            Message::message("
<div class=\"alert alert-info\">
  <strong>Deleted!</strong> Data has not been deleted successfully.
</div>");
            Utility::redirect("index.php");
        }

    }

    public function trash()
    {
        $this->deleted_at = time();
        $query = "UPDATE `atomic_project`.`hobbies` SET `deleted_at` =" . $this->deleted_at . " WHERE `hobbies`.`id` = " . $this->id;
        $result = mysqli_query($this->conn, $query);
        if ($result) {
            Message::message("
<div class=\"alert alert-info\">
  <strong>Deleted!</strong> Data has been trashed successfully.
</div>");
            Utility::redirect("index.php");
        } else {
            Message::message("
<div class=\"alert alert-info\">
  <strong>Deleted!</strong> Data has not been trashed successfully.
</div>");
            Utility::redirect("index.php");
        }

    }

    public function trashed()
    {
        $_allHobbies = array();
        $query = "SELECT * FROM `hobbies` WHERE `deleted_at` IS NOT NULL";
        $result = mysqli_query($this->conn, $query);
        while ($row = mysqli_fetch_object($result)) {
            $_allHobbies[] = $row;
        }

        return $_allHobbies;


    }

    public function recover()
    {

        $query = "UPDATE `atomic_project`.`hobbies` SET `deleted_at` = NULL WHERE `hobbies`.`id` = " . $this->id;
        $result = mysqli_query($this->conn, $query);
        if ($result) {
            Message::message("
<div class=\"alert alert-info\">
  <strong>Deleted!</strong> Data has been recovered successfully.
</div>");
            Utility::redirect("index.php");
        } else {
            Message::message("
<div class=\"alert alert-info\">
  <strong>Deleted!</strong> Data has not been recovered successfully.
</div>");
            Utility::redirect("index.php");
        }

    }
    public function recoverSeleted($IDs = Array())
    {
        if ((is_array($IDs)) && (count($IDs > 0))) {
            $ids = implode(",", $IDs);
            $query = "UPDATE `atomic_project`.`hobbies` SET `deleted_at` = NULL WHERE `hobbies`.`id` IN(" . $ids . ")";
            //echo $query;
            //die();
            $result = mysqli_query($this->conn, $query);
            if ($result) {
                Message::message("
<div class=\"alert alert-info\">
  <strong>Deleted!</strong> Selected Data has been recovered successfully.
</div>");
                Utility::redirect("index.php");
            } else {
                Message::message("
<div class=\"alert alert-info\">
  <strong>Deleted!</strong> Selected Data has not been recovered successfully.
</div>");
                Utility::redirect("index.php");
            }

        }

    }

    public function deleteMultiple($IDs = Array())
    {
        if ((is_array($IDs)) && (count($IDs > 0))) {
            $ids = implode(",", $IDs);
            $query = "DELETE FROM `atomic_project`.`hobbies` WHERE `hobbies`.`id`  IN(" . $ids . ")";
            $result = mysqli_query($this->conn, $query);
            if ($result) {
                Message::message("
<div class=\"alert alert-info\">
  <strong>Deleted!</strong> Selected Data has been deleted successfully.
</div>");
                Utility::redirect("index.php");
            } else {
                Message::message("
<div class=\"alert alert-info\">
  <strong>Deleted!</strong> Selected Data has not been deleted successfully.
</div>");
                Utility::redirect("index.php");
            }

        }
    }

    public function count(){
        $query="SELECT COUNT(*) AS totalItem FROM `atomic_project`.`hobbies` WHERE `deleted_at` IS NULL ";
        $result=mysqli_query($this->conn,$query);
        $row= mysqli_fetch_assoc($result);
        return $row['totalItem'];
    }



    public function paginator($pageStartFrom=0,$Limit=5){
        $_allBook = array();
        $query="SELECT * FROM `hobbies` WHERE `deleted_at` IS NULL LIMIT ".$pageStartFrom.",".$Limit;
        $result = mysqli_query($this->conn, $query);
        while ($row = mysqli_fetch_object($result)) {
            $_allBook[] = $row;
        }

        return $_allBook;

    }
}