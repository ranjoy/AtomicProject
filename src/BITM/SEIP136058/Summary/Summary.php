<?php
namespace App\Bitm\SEIP136058\Summary;
use App\Bitm\SEIP136058\Utility\Utility;
use App\Bitm\SEIP136058\Message\Message;

class Summary{
    public $id="";
    public $org_name="";
    public $summary="";
    public $deleted_at="";
    public $conn;



    public function __construct()
    {
        $this->conn = mysqli_connect("localhost", "root", "", "atomic_project") or die("Database connection failed");
    }
    public function prepare($data = "")
    {
        if (array_key_exists("name", $data)) {
            $this->org_name = $data['name'];
        }


        if (array_key_exists("summary", $data)) {
            $this->summary = $data['summary'];
        }
        if (array_key_exists("id", $data)) {
            $this->id = $data['id'];
        }


    }

    public function store(){
        $query="INSERT INTO `atomic_project`.`summary` (`organization`, `summary`) VALUES ('".$this->org_name."', '".$this->summary."')";

        $result = mysqli_query($this->conn, $query);
        if ($result) {
            Message::message("
                <div class=\"alert alert-success\">
                            <strong>Success!</strong> Data has been stored successfully.
                </div>");
            Utility::redirect("index.php");
        } else {
            echo "Error";
        }

    }

    public function count(){
        $query="SELECT COUNT(*) AS totalItem FROM `atomic_project`.`summary` WHERE `deleted_at` IS NULL ";
        $result=mysqli_query($this->conn,$query);
        $row= mysqli_fetch_assoc($result);
        return $row['totalItem'];
    }



    public function paginator($pageStartFrom=0,$Limit=5){
        $_allSummary = array();
        $query="SELECT * FROM `summary` WHERE `deleted_at` IS NULL LIMIT ".$pageStartFrom.",".$Limit;
        $result = mysqli_query($this->conn, $query);
        while ($row = mysqli_fetch_object($result)) {
            $_allSummary[] = $row;
        }

        return $_allSummary;

    }




    public function view()
    {
        $query = "SELECT * FROM `summary` WHERE `id`=" . $this->id;
        $result = mysqli_query($this->conn, $query);
        //echo $result;
        $row = mysqli_fetch_object($result);

        return $row;
    }

    public function update()
    {
        if (!empty($this->org_name)) {
            $query = "UPDATE `atomic_project`.`summary` SET `organization` = '" . $this->org_name . "', `summary` = '" . $this->summary . "' WHERE `summary`.`id` =" . $this->id;
        } else {
            $query = "UPDATE `atomic_project`.`summary` SET `organization` = '" . $this->org_name . "' WHERE `summary`.`id` =" . $this->id;

        }

        $result = mysqli_query($this->conn, $query);
        if ($result) {
            Message::message("
<div class=\"alert alert-info\">
  <strong>Success!</strong> Data has been updated  successfully.
</div>");
            Utility::redirect("index.php");
        } else {
            echo "Error";
        }
    }

    public function delete()
    {
        $query = "DELETE FROM `atomic_project`.`summary` WHERE `summary`.`id` = " . $this->id;
        $result = mysqli_query($this->conn, $query);
        if ($result) {
            Message::message("
<div class=\"alert alert-info\">
  <strong>Deleted!</strong> Data has been deleted successfully.
</div>");
            Utility::redirect("index.php");
        } else {
            Message::message("
<div class=\"alert alert-info\">
  <strong>Deleted!</strong> Data has not been deleted successfully.
</div>");
            Utility::redirect("index.php");
        }

    }

    public function trash()
    {
        $this->deleted_at = time();
        $query = "UPDATE `atomic_project`.`summary` SET `deleted_at` =" . $this->deleted_at . " WHERE `summary`.`id` = " . $this->id;
        $result = mysqli_query($this->conn, $query);
        if ($result) {
            Message::message("<div class=\"alert alert-info\"><strong>Deleted!</strong>
                                Data has been trashed successfully.
                                </div>");
            Utility::redirect("index.php");
        } else {
            Message::message("<div class=\"alert alert-info\">
                    <strong>Deleted!</strong>Data has not been trashed successfully.
                        </div>");
            Utility::redirect("index.php");
        }

    }

    public function trashed()
    {
        $_allTS = array();
        $query = "SELECT * FROM `summary` WHERE `deleted_at` IS NOT NULL";
        $result = mysqli_query($this->conn, $query);
        //echo $result;
        while ($row = mysqli_fetch_object($result)) {
            $_allTS[] = $row;
        }

        return $_allTS;


    }
    public function recover()
    {

        $query = "UPDATE `atomic_project`.`summary` SET `deleted_at` = NULL WHERE `summary`.`id` = " . $this->id;
        $result = mysqli_query($this->conn, $query);
        if ($result) {
            Message::message("
<div class=\"alert alert-info\">
  <strong>Deleted!</strong> Data has been recovered successfully.
</div>");
            Utility::redirect("index.php");
        } else {
            Message::message("
<div class=\"alert alert-info\">
  <strong>Deleted!</strong> Data has not been recovered successfully.
</div>");
            Utility::redirect("index.php");
        }

    }

    public function deleteMultiple($IDs){
        //Utility::dd($IDs);
        if(count($IDs)>0){
            $ids = implode(",",$IDs);
            $query = "DELETE FROM `summary` WHERE `id` IN (".$ids.")";
            $result = mysqli_query($this->conn,$query);
            if($result){
                Message::message("
<div class=\"alert alert-info\">
  <strong>Deleted!</strong>Data has been deleted successfully
  </div>");
                Utility::redirect("index.php");
            }

            else{

                Message::message("Error in deleting data");
                Utility::redirect("index.php");
            }
        }
    }
    public function recovermultiple($IDs = Array())
    {
        if ((is_array($IDs)) && (count($IDs > 0))) {
            $ids = implode(",", $IDs);
            $query = "UPDATE `atomic_project`.`summary` SET `deleted_at` = NULL WHERE `summary`.`id` IN(" . $ids . ")";
            //echo $query;
            //die();
            $result = mysqli_query($this->conn, $query);
            if ($result) {
                Message::message("
<div class=\"alert alert-info\">
  <strong>Deleted!</strong> Selected Data has been recovered successfully.
</div>");
                Utility::redirect("index.php");
            } else {
                Message::message("
<div class=\"alert alert-info\">
  <strong>Deleted!</strong> Selected Data has not been recovered successfully.
</div>");
                Utility::redirect("index.php");
            }

        }
    }
}