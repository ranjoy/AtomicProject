<?php
//var_dump($_GET);
include_once ('../../../vendor/autoload.php');
use App\BITM\SEIP136058\Email\Email;
use App\BITM\SEIP136058\Utility\Utility;

$email= new Email();
$email->prepare($_GET);
$single_info=$email->view();
//Utility::d($singleItem);
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Email </title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h2>edit</h2>
    <form role="form" action="update.php" method="post" enctype="multipart/form-data">
        <input type="hidden" name="id" value="<?php echo $single_info->id?>">
        <div class="form-group">
            <label>Name:</label>
            <input type="text" class="form-control"name="name" value="<?php echo $single_info->username?>">
            <input type="text" class="form-control"name="email" value="<?php echo $single_info->email?>" >
        </div>

        <input type="submit" value="Update">
    </form>
</div>

</body>
</html>

