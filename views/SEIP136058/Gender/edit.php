<?php

include_once ('../../../vendor/autoload.php');
use App\Bitm\SEIP136058\Gender\Gender;


$gender= new Gender();
$gender->prepare($_GET);
$singleItem=$gender->view();
//var_dump($singleItem);

?>

<!DOCTYPE html>

<html>
<head>
    <title>Select Gender</title>

</head>
<body>
<div class="cont">
    <form role="form" method="post" action="update.php">
        <div class="form-group">
            <label>Edit User Name:</label>
            <input type="hidden" name="id" id="title" value="<?php echo $singleItem->id?>">
            <input type="text" name="name" class="form-control"  value="<?php echo $singleItem->username?>">
        </div>
<div class="container">
    <h2><strong>Select your gender</strong></h2>

    <div class="checkbox">
        <label><input type="radio" name="gender" value="Male" <?php if($singleItem->gender=="Male"):?>checked<?php endif ?>>Male</label>
    </div>
    <div class="checkbox">
        <label><input type="radio" name="gender" value="Female"  <?php if($singleItem->gender=="Female"):?>checked<?php endif ?> >Female</label>
    </div>
    <div class="checkbox">
        <label><input type="radio" name="gender" value="Others"  <?php if($singleItem->gender=="Others"):?>checked<?php endif ?>>Others</label>
    </div>
    <br/>
    <br/>
    <button type="submit" class="btn btn-default">Update</button>


    </form>
</div>

</body>
</html>





