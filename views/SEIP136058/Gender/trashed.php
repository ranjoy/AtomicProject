<?php

include_once('../../../vendor/autoload.php');
use App\BITM\SEIP136058\Gender\Gender;
use App\BITM\SEIP136058\Utility\Utility;
use App\BITM\SEIP136058\Message\Message;

$object= new Gender();
$allT=$object->trashed();

//var_dump($allBook);
//Utility::d($allBook);



?>

<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="../../../Resources/bootstrap/css/bootstrap.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <!--  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>-->
</head>
<body>

<div class="container">
    <h2>Trash List</h2>

    <a href="index.php" class="btn btn-info" role="button">View User's Gender</a><br><br/>

    <form action="recovermultiple.php" method="post" id="multiple">
        <button type="submit" class="btn btn-info">Recover Selected</button>
        <button type="button" class="btn btn-primary" id="delete">Delete all Selected</button>
        <br><br>
        <div class="table-responsive">
            <table class="table">
                <thead>
                <tr>
                    <th>Select</th>
                    <th>SL</th>
                    <th>ID</th>
                    <th>User Name</th>
                    <th>Gender</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                <?php
                $sl=0;
                foreach($allT as $info){
                    $sl++;
                    ?>
                    <tr>
                        <td><input type="checkbox" name="mark[]" value="<?php echo $info->id?>"></td>
                        <td><?php echo $sl?></td>
                        <td><?php echo $info-> id?></td>
                        <td><?php echo $info->username?></td>
                        <td><?php echo $info->gender?></td>
                        <td><a href="recover.php?id=<?php echo $info-> id?>" class="btn btn-primary" role="button">Recover</a>
                            <a href="delete.php?id=<?php echo $info->id?>" class="btn btn-danger" role="button" id="delete"  Onclick="return ConfirmDelete()">Delete</a>
                        </td>


                    </tr>
                <?php } ?>


                </tbody>
            </table>
    </form>
</div>
</div>
<script>
    $('#delete').on('click',function(){
        document.forms[0].action="deletemultiple.php";
        $('#multiple').submit();
    });

    function ConfirmDelete()
    {
        var x = confirm("Are you sure you want to delete?");
        if (x)
            return true;
        else
            return false;
    }
</script>


</body>
</html>
