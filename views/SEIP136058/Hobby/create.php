<!DOCTYPE html>
<html lang="en">
<head>
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h2>Hobbies</h2>

    <form role="form" method="post" action="store.php">

        <div class="checkbox">
            <label><input type="checkbox" name=hobby[] value="Playing Cricket">Playing Cricket</label>
        </div>
        <div class="checkbox disabled">
            <label><input type="checkbox" name=hobby[] value="Playing Football">Playing Football</label>
        </div>
        <div class="checkbox">
            <label><input type="checkbox" name=hobby[] value="Gaming">Gaming</label>
        </div>
        <div class="checkbox">
            <label><input type="checkbox" name=hobby[] value="Watching Movies">Watching Movies</label>
        </div>
        <div class="checkbox">
            <label><input type="checkbox" name=hobby[] value="Hanging Out">Hanging Out</label>
        </div>
        <input type="submit" value="Submit">
    </form>
</div>

</body>
</html>

