<!DOCTYPE html>
<html lang="en">
<head>
    <title>City</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h2>User City</h2>

    <form role="form" method="post" action="store.php">
        <label>User name:</label>
        <input type="text" name="name">

        <div class="form-group">
            <label for="sel1">Select Your city</label>
            <select class="form-control" id="sel1" name="city">
                <option>Barisal</option>
                <option>Chittagong</option>
                <option>Dhaka</option>
                <option>Khulna</option>
                <option>Rajshahi</option>
                <option>Rangpur</option>
                <option>Sylhet</option>
            </select>
        </div>

        <input type="submit" value="save">
    </form>
</div>

</body>
</html>

