<?php
//var_dump($_GET);
include_once ('../../../vendor/autoload.php');
use App\BITM\SEIP136058\Summary\Summary;
use App\BITM\SEIP136058\Book\Utility;

$object= new Summary();
$object->prepare($_GET);
$singleItem=$object->view();
//Utility::d($singleItem);
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>Edit Summary</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h2>Edit Organization's Summary</h2>
    <form role="form" method="post" action="update.php">
        <label>Organization name:</label>
        <input type="hidden" name="id" id="title" value="<?php echo $singleItem->id?>">
        <input type="text" name="name" value="<?php echo $singleItem->organization?>">
        <div class="form-group">
            <label for="comment">Summary:</label>
            <textarea class="form-control" rows="12" id="comment" name="summary" ><?php echo $singleItem->summary?></textarea>
        </div>
        <input type="submit" value="Update">
    </form>
</div>

</body>
</html>
