<?php
//var_dump($_GET);
include_once ('../../../vendor/autoload.php');
use App\Bitm\SEIP136058\Summary\Summary;
use App\Bitm\SEIP136058\Utility\Utility;

$summary= new Summary();
$summary->prepare($_GET);
//var_dump($_GET);
$singleItem=$summary->view();
//var_dump($singleItem);
//Utility::d($singleItem);
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Summary</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<body>
<br/>

<div class="container">
    <h2><?php echo $singleItem->organization?></h2>
    <ul class="list-group">
        <li class="list-group-item">ID: <?php echo $singleItem->id?></li>
        <li class="list-group-item">Organization Name: <?php echo $singleItem->organization?></li>
    </ul>
    <label>Summary</label>
    <textarea class="form-control" rows="10" id="comment" name="summary"><?php echo $singleItem->summary?></textarea>
    <br>
    <a href="index.php" class="btn btn-primary" role="button">View List</a>
</div>


</body>
</html>