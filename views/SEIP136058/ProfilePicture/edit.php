<?php
include_once('../../../vendor/autoload.php');
use App\BITM\SEIP136058\ProfilePicture\ImageUploader;

$profile_picture= new ImageUploader();
$single_info=$profile_picture->prepare($_GET)->view();


?>




<!DOCTYPE html>
<html lang="en">
<head>
    <title>Profile </title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h2>Create Profile</h2>
    <form role="form" action="update.php" method="post" enctype="multipart/form-data">
        <input type="hidden" name="id" value="<?php echo $single_info->id?>">
        <div class="form-group">
            <label>Name:</label>
            <input type="text" class="form-control"name="name" value="<?php echo $single_info->name?>" >
        </div>
        <div class="form-group">
            <label for="pwd">Choose new profile picture:</label>
            <input type="file" name="image" class="form-control">
            <img src="../../../Resources/Images/<?php echo $single_info->images?>"alt="image" height="250px" width="250px" class="img-responsive">
        </div>
        <input type="submit" value="Update">
    </form>
</div>

</body>
</html>

