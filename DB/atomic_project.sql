-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jun 30, 2016 at 08:49 AM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 5.6.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `atomic_project`
--

-- --------------------------------------------------------

--
-- Table structure for table `birthday`
--

CREATE TABLE `birthday` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `date` date NOT NULL,
  `deleted_at` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `birthday`
--

INSERT INTO `birthday` (`id`, `name`, `date`, `deleted_at`) VALUES
(8, 'ranjoy', '1992-12-02', '1467267179'),
(9, 'aharna', '1992-12-11', NULL),
(10, 'gfff', '1992-12-12', '1467238264'),
(11, 'trtert', '1111-11-11', NULL),
(12, 'erer', '0002-02-22', NULL),
(13, 'vutung', '1992-06-02', NULL),
(14, 'fdf', '1997-03-03', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `book`
--

CREATE TABLE `book` (
  `id` int(11) NOT NULL,
  `title` varchar(100) NOT NULL,
  `deleted_at` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `book`
--

INSERT INTO `book` (`id`, `title`, `deleted_at`) VALUES
(19, 'à¦¶à§à¦­à§à¦°', NULL),
(20, 'à¦¹à¦¿à¦®à§', NULL),
(21, 'harry poter', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `cities`
--

CREATE TABLE `cities` (
  `id` int(11) NOT NULL,
  `username` varchar(100) NOT NULL,
  `city` varchar(100) NOT NULL,
  `deleted_at` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cities`
--

INSERT INTO `cities` (`id`, `username`, `city`, `deleted_at`) VALUES
(1, 'Mohammed Armanuzzaman Chowdhury Suzan', 'Chittagong', NULL),
(2, 'Rita Das', 'Barisal', NULL),
(3, 'Samia Zaman Mim', 'Khulna', NULL),
(4, 'Huraon Jannat', 'Chittagong', NULL),
(5, 'Emam Hossain Fahad', 'Chittagong', '1467236660'),
(7, 'ranjoy', 'Chittagong', NULL),
(8, 'afjasjf', 'Dhaka', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `gender`
--

CREATE TABLE `gender` (
  `id` int(11) NOT NULL,
  `username` varchar(100) NOT NULL,
  `gender` varchar(100) NOT NULL,
  `deleted_at` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gender`
--

INSERT INTO `gender` (`id`, `username`, `gender`, `deleted_at`) VALUES
(1, 'Arman', 'Male', NULL),
(2, 'Rita', 'Female', NULL),
(3, 'Samia Zaman', 'Female', NULL),
(4, 'istiak', 'Others', NULL),
(5, 'aharna', 'Female', NULL),
(6, 'nasrin', 'Female', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `hobbies`
--

CREATE TABLE `hobbies` (
  `id` int(11) NOT NULL,
  `hobby` varchar(200) NOT NULL,
  `deleted_at` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `hobbies`
--

INSERT INTO `hobbies` (`id`, `hobby`, `deleted_at`) VALUES
(14, 'Gaming,Watching Movies,Hanging Out', '1467233044'),
(16, 'Playing Cricket,Playing Football', NULL),
(17, 'Gaming,Playing Cricket,Playing Football,Watching Movies', NULL),
(18, 'Playing Cricket,Playing Football,Gaming', NULL),
(22, 'Gaming,Watching Movies', NULL),
(23, 'Playing Cricket,Playing Football,Gaming', NULL),
(24, 'Playing Cricket,Playing Football,Gaming,Watching Movies,Hanging Out', NULL),
(25, 'Playing Football', NULL),
(26, 'Watching Movies', NULL),
(27, 'Gaming', NULL),
(28, 'Gaming', NULL),
(29, 'Watching Movies', NULL),
(30, 'Watching Movies', NULL),
(31, 'Watching Movies,Hanging Out', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `profilepicture`
--

CREATE TABLE `profilepicture` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `images` varchar(100) NOT NULL,
  `deleted_at` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `profilepicture`
--

INSERT INTO `profilepicture` (`id`, `name`, `images`, `deleted_at`) VALUES
(5, 'bee', '1467241641bee.jpg', '1467241657'),
(8, 'bee', '1467242361bee.jpg', '1467242738'),
(9, 'jerry', '1467242575jerry.jpg', NULL),
(10, 'fkdafhdsa', '1467242610bee.jpg', NULL),
(11, 'er', '1467242626tom.jpg', NULL),
(12, 'fgrr', '1467242652jerry.jpg', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `subscribers`
--

CREATE TABLE `subscribers` (
  `id` int(11) NOT NULL,
  `username` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `deleted_at` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `subscribers`
--

INSERT INTO `subscribers` (`id`, `username`, `email`, `deleted_at`) VALUES
(1, 'Arman', 'arman.cu.cse@gmail.com', NULL),
(3, 'Rita Das', 'ritadascucse@gmail.com', NULL),
(5, 'Huraon Jannat', 'huraon.cu.cse@gmail.com', NULL),
(7, 'yojnar', 'ranjoy_666@yahoo.com', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `summary`
--

CREATE TABLE `summary` (
  `id` int(11) NOT NULL,
  `organization` varchar(100) NOT NULL,
  `summary` varchar(255) NOT NULL,
  `deleted_at` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `summary`
--

INSERT INTO `summary` (`id`, `organization`, `summary`, `deleted_at`) VALUES
(1, 'CSE', 'Department of Computer Science & Engineering, University of Chittagong', NULL),
(2, 'Giving hand', 'We are working for the helpless,foodless,orphan child. We belive in miracle.We belive every child is special. ', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `birthday`
--
ALTER TABLE `birthday`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `book`
--
ALTER TABLE `book`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cities`
--
ALTER TABLE `cities`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gender`
--
ALTER TABLE `gender`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hobbies`
--
ALTER TABLE `hobbies`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `profilepicture`
--
ALTER TABLE `profilepicture`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subscribers`
--
ALTER TABLE `subscribers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `summary`
--
ALTER TABLE `summary`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `birthday`
--
ALTER TABLE `birthday`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `book`
--
ALTER TABLE `book`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT for table `cities`
--
ALTER TABLE `cities`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `gender`
--
ALTER TABLE `gender`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `hobbies`
--
ALTER TABLE `hobbies`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;
--
-- AUTO_INCREMENT for table `profilepicture`
--
ALTER TABLE `profilepicture`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `subscribers`
--
ALTER TABLE `subscribers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `summary`
--
ALTER TABLE `summary`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
